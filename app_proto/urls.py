from django.urls import include, path
from app_proto.views import index,index2,index3,index4

urlpatterns = [
    path('', index, name='home'),
    path('profile', index2, name='profile'),
    path('contact', index3, name='contact'),
    path('forms', index4, name='forms')]
