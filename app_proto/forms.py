from django import forms
from .models import Friends, Year
from crispy_forms.helper import FormHelper
from crispy_forms.layout  import Layout, Submit

class FriendsForm(forms.ModelForm):

    class Meta:
        model = Friends
        fields = ('year', 'name', 'hobby', 'fav_food', 'fav_drink')