from django.db import models

# Create your models here.

class Year(models.Model):
    yearSelection=models.TextChoices('Year', '2016 2017 2018 2019 Other')
    year=models.CharField(blank=True, choices=yearSelection.choices, max_length=10)

    def __str__(self):
        return self.year

class Friends(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    hobby = models.CharField(max_length=100)
    fav_food = models.CharField(max_length=50)
    fav_drink = models.CharField(max_length=50)

    def __str__(self):
        return self.name