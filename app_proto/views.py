from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from .forms import FriendsForm, Year
from .models import Friends

# Create your views here.

def index(request):
    return render(request, 'home.html')

def index2(request):
    return render(request, 'profile.html')

def index3(request):
    return render(request, 'contact.html')

def index4(request):
    friend = Friends.objects.all()
    form = FriendsForm()
    if request.method == 'POST':
        form = FriendsForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            form= FriendsForm()
    return render(request, 'forms.html', {'form': form, 'friends': friend})
